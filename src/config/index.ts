import { ConnectionOptions } from 'mongoose';

const appData = require('../../package.json');

interface Config {
  version: string;
  name: string;
  description: string;
  session: {
    domain: string;
    secret: string;
    timeout: number;
  };
  email: {
    enabled: boolean,
    service: string,
    username: string,
    password: string
    from: string
  }
}

export const config: Config = {
  version: appData.version,
  name: appData.name,
  description: appData.description,
  session: {
    domain: process.env.APP_SESSION_DOMAIN,
    secret: process.env.APP_SESSION_SECRET,
    timeout: parseInt(process.env.APP_SESSION_TIMEOUT, 10),
  },
  email: {
    enabled: process.env.EMAIL_ENABLED == 'true',
    service: process.env.EMAIL_SERVICE,
    username: process.env.EMAIL_USERNAME,
    password: process.env.EMAIL_PASSWORD,
    from: process.env.EMAIL_FROM
  }
};
