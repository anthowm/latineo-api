import { Injectable } from "@nestjs/common";
import shortid = require("shortid");
import * as fs from "fs";
import * as path from "path";


@Injectable()
export class UtilsService {
    readonly UPLOAD_DIR = './images';
    constructor() { }
    /**
     *
     *
     * @param {*} stream
     * @param {*} filename
     * @param {*} mimetype
     * @returns {Promise<any>}
     * @memberof UtilsService
     */
    storeFs(stream: any, filename: any, mimetype: any): Promise<any> {
        const id = shortid.generate();
        filename = `${id}-${filename}`
        const path = `${this.UPLOAD_DIR}/${filename}`;
        return new Promise((resolve, reject) =>
            stream
                .on('error', error => {
                    if (stream.truncated)
                        // Delete the truncated file.
                        fs.unlinkSync(path);
                    reject(error)
                })
                .pipe(fs.createWriteStream(path))
                .on('error', error => {
                    reject(error)
                })
                .on('finish', () => resolve({ id, filename, path, mimetype }))
        );
    }

    /**
     *
     *
     * @param {*} upload
     * @returns
     * @memberof UtilsService
     */
    async processUpload(upload: any) {
        const { createReadStream, filename, mimetype } = await upload;
        const stream = createReadStream();
        return this.storeFs(stream, filename, mimetype);
    }

    /**
     *
     *
     * @param {String} value
     * @param {String} min
     * @param {String} max
     * @returns {String}
     * @memberof UtilsService
     */
    lengthBetween(value: String, min: String, max: String): String {
        return `${value} must be between ${min} and ${max} characters`;
    }

    /**
     *
     *
     * @param {*} file
     * @returns {Boolean}
     * @memberof UtilsService
     */
    fileFilter(file: any): Boolean {
        if (
            file.mimetype === 'image/png' ||
            file.mimetype === 'image/jpg' ||
            file.mimetype === 'image/jpeg'
        ) {
            return true;
        } else {
            return false;
        }
    };

    /**
     *
     *
     * @param {fs.PathLike} filePath
     * @memberof UtilsService
     */
    clearImage(filePath: fs.PathLike): void {
        filePath = path.join(__dirname, '..', '../' + filePath);
        fs.unlink(filePath, err => {

        });
    }
}
