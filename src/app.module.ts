import { Module } from '@nestjs/common';
import { ConfigModule } from "./config/config.module";
import { PetsModule } from "./pet/pets.module";
import { UsersModule } from './user/users.module';
import { GraphQLModule } from '@nestjs/graphql';

import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { GlobalModule } from './global/global.module';
import { ConfigService } from './config/config.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLUpload } from 'graphql-upload';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.mongoUri,
        useNewUrlParser: true
      }),
      inject: [ConfigService],
    }),
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      // resolvers: { Upload: GraphQLUpload },
      installSubscriptionHandlers: true,
      context: ({ req }) => {
        return { req };
      },
      playground: true,
      definitions: {
        path: join(process.cwd(), './src/graphql.classes.ts'),
        outputAs: 'class',
      },
      uploads: {
        maxFileSize: 10000000, // 10 MB
        maxFiles: 5
      }
    }),
    GlobalModule,
    UsersModule,
    AuthModule,
    PetsModule,
    ConfigModule
  ],
})
export class AppModule { }
