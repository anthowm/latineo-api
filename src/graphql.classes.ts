
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class CreateUserInput {
    fullName: string;
    username: string;
    email: string;
    terms: boolean;
    password: string;
}

export class LoginUserInput {
    username?: string;
    email?: string;
    password: string;
}

export class PetInputData {
    name: string;
    type: string;
    imageUrls: Upload[];
}

export class PetInputDataUpdate {
    name: string;
    type: string;
    imageUrls?: Upload[];
}

export class UpdateUserInput {
    username?: string;
    email?: string;
    password?: string;
    enabled?: boolean;
}

export class File {
    id: string;
    path: string;
    filename: string;
    mimetype: string;
}

export class FilterDataPet {
    isFilterData: boolean;
    currentPage: number;
    queryParams?: QueryParamsPet;
}

export class LoginResult {
    user: User;
    token: string;
}

export abstract class IMutation {
    abstract createPet(petInputData?: PetInputData): Pet | Promise<Pet>;

    abstract updatePet(id: MongoObjectId, petInputDataUpdate?: PetInputDataUpdate): Pet | Promise<Pet>;

    abstract deletePet(id: MongoObjectId): boolean | Promise<boolean>;

    abstract createUser(createUserInput?: CreateUserInput): User | Promise<User>;

    abstract updateUser(username: string, fieldsToUpdate: UpdateUserInput): User | Promise<User>;

    abstract resetPassword(username: string, code: string, password: string): User | Promise<User>;
}

export class Pet {
    _id: MongoObjectId;
    name: string;
    type: string;
    imageUrls: File[];
    creator: UserPet;
    createdAt: Date;
    updatedAt: Date;
}

export class PetData {
    pets: Pet[];
    totalPets: number;
    filterData: FilterDataPet;
}

export abstract class IQuery {
    abstract login(user: LoginUserInput): LoginResult | Promise<LoginResult>;

    abstract refreshToken(username: string): string | Promise<string>;

    abstract pets(page?: number, name?: string, type?: string): PetData | Promise<PetData>;

    abstract pet(id: MongoObjectId): Pet | Promise<Pet>;

    abstract users(): User[] | Promise<User[]>;

    abstract user(username?: string, email?: string): User | Promise<User>;

    abstract forgotPassword(email?: string): boolean | Promise<boolean>;

    abstract temp__(): boolean | Promise<boolean>;
}

export class QueryParamsPet {
    name?: string;
    type?: string;
}

export class User {
    username: string;
    fullName: string;
    email: string;
    terms: boolean;
    createdAt: Date;
    updatedAt: Date;
    enabled: boolean;
    pets: Pet[];
    _id: MongoObjectId;
}

export class UserPet {
    _id: MongoObjectId;
    name: string;
    email: string;
    pets: Pet[];
}

export type Date = any;
export type MongoObjectId = any;
export type Upload = any;
