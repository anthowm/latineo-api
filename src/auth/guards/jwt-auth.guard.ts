import { Injectable, ExecutionContext } from '@nestjs/common';
import { ConfigService } from "./../../config/config.service";
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthenticationError } from 'apollo-server-core';
import * as jwt from 'jsonwebtoken';
import { config } from '../../config';
@Injectable()
// In order to use AuthGuard together with GraphQL, you have to extend
// the built-in AuthGuard class and override getRequest() method.
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(private readonly configService: ConfigService) {
        super();
    }
    getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        const request = ctx.getContext().req;
        const authHeader = request.get('Authorization');
        const token = authHeader.split(' ')[1];
        const decodedToken: any = jwt.verify(token, this.configService.jwtSecret);
        request.userId = decodedToken.userId;
        return request;
    }

    handleRequest(err: any, user: any, info: any) {
        if (err || !user) {
            throw err || new AuthenticationError('Could not authenticate with token');
        }
        return user;
    }
}
