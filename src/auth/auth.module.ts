import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '../user/users.module';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { AuthResolver } from './auth.resolver';

@Module({
	imports: [
		PassportModule.register({ defaultStrategy: 'jwt' }),
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: (configService: ConfigService) => {
				const options: JwtModuleOptions = {
					secretOrPrivateKey: configService.jwtSecret,
				};
				if (configService.jwtExpiresIn) {
					options.signOptions = {
						expiresIn: configService.jwtExpiresIn,
					};
				}
				return options;
			},
			inject: [ConfigService],
		}),
		UsersModule,
	],
	providers: [AuthService, AuthResolver, JwtStrategy],
	exports: [PassportModule, AuthService]
})
export class AuthModule {
}
