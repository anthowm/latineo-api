
import { UsersService } from "./../user/users.service";
import { AuthService } from "./auth.service";
import { Query, Resolver, Args } from "@nestjs/graphql";
import { LoginUserInput, LoginResult } from "../graphql.classes";
import { AuthenticationError } from "apollo-server-core";
import { JwtAuthGuard } from "./guards/jwt-auth.guard";
import { UseGuards } from "@nestjs/common";

@Resolver('Auth')
export class AuthResolver {
    constructor(
        private authService: AuthService,
        private usersService: UsersService
    ) { }

    @Query('login')
    async login(@Args('user') user: LoginUserInput): Promise<LoginResult> {
        const result = await this.authService.validateUserByPassword(user);
        if (result) return result;
        console.log("TCL: AuthResolver -> result", result)
        throw new AuthenticationError(
            'Could not log-in with the provided credentials',
        );
    }

    @Query('refreshToken')
    @UseGuards(JwtAuthGuard)
    async refreshToken(@Args('username') username: string): Promise<string> {
        const user = await this.usersService.findOneByUsername(username);
        console.log("TCL: AuthResolver -> user", user)
        if (!user) {
            throw new AuthenticationError(
                'Could not log-in with the provided credentials',
            );
        }

        const result = await this.authService.createJwt(user);
        if (result) return result.token;
        console.log("TCL: AuthResolver -> result", result)
        throw new AuthenticationError(
            'Could not log-in with the provided credentials',
        );
    }
}