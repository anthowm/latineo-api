export interface JwtPayload {
	email: string;
	username: string;
	userId: string;
	expiration?: Date;
}
