import { Global, Module } from "@nestjs/common";
import { UtilsService } from "../utils/utils.service";

@Global()
@Module({
    imports: [
        UtilsService
    ],
    exports: [UtilsService]
})
export class GlobalModule { }