import { Injectable } from "@nestjs/common";
import { PetInputDataUpdate } from "./../graphql.classes";
import { UtilsService } from "./../utils/utils.service";

import { PetInputData } from "./../graphql.classes";

import { Model } from "mongoose";
import { PetDocument } from "./models/pet.schema";
import { ApolloError } from "apollo-server-core";
import { createPetInterface } from "./interfaces/pet.interface";
import { InjectModel } from "@nestjs/mongoose";


@Injectable()
export class PetsService {
    constructor(
        @InjectModel('Pet') private readonly petModel: Model<PetDocument>,
        private readonly utilsService: UtilsService
    ) { }


    /**
     * Returns total pets by a filter or undefined
     *
     * @param {{ name: Object, type: Object }} filter
     * @returns {(Promise<number | undefined>)}
     * @memberof PetsService
     */
    async getTotalPets(filter?: { name: Object, type: Object }): Promise<number | undefined> {
        const totalPets = this.petModel
            .find(filter ? filter : {})
            .countDocuments()
            .exec();
        if (totalPets) return totalPets;
        return undefined;
    }

    /**
     * Returns pets by a filter or undefined
     *
     * @param {number} page
     * @param {number} [perPage=8]
     * @param {{ name: Object, type: Object }} [filter]
     * @returns
     * @memberof PetsService
     */
    async getAllpets(page: number, perPage = 8, filter?: { name: Object, type: Object }): Promise<PetDocument[] | undefined> {
        const pets = await this.petModel
            .find(filter ? filter : {})
            .sort({ createdAt: -1 })
            .skip((page - 1) * perPage)
            .limit(perPage)
            .populate('creator')
            .exec();
        if (pets) return pets;
        return undefined;
    }

    /**
     * Returns pet by id or throw exception
     *
     * @param {number} id
     * @returns
     * @memberof PetsService
     */
    async findPetById(id: number): Promise<PetDocument> {
        const pet = await this.petModel
            .findById(id)
            .populate('creator');
        if (pet) return pet;
        throw new ApolloError('No pet found!.', '404');
    }

    /**
     * Return a delete pet or throw exception
     *
     * @param {number} id
     * @returns {Promise<PetDocument>}
     * @memberof PetsService
     */
    async findPetByIdAndRemove(id: number): Promise<PetDocument> {
        const deletePet = await this.petModel.findByIdAndRemove(id);
        if (deletePet) return deletePet;
        throw new ApolloError('No pet found for delete!.', '404');
    }


    /**
     *
     *
     * @param {createPetInterface} petInputData
     * @returns {Promise<PetDocument>}
     * @memberof PetsService
     */
    async createPet(petInputData: createPetInterface): Promise<PetDocument> {
        const createdPet = new this.petModel(petInputData);
        let create: PetDocument | undefined;
        try {
            create = await createdPet.save();
        } catch (error) {
            throw new ApolloError("Could not create Pet", "403");
        }
        return create;
    }

    /**
     *
     *
     * @param {PetInputData} petInputData
     * @param {*} imagesUploaded
     * @memberof PetsService
     */
    validationPet(petInputData: PetInputData | PetInputDataUpdate, imagesUploaded: any): void {
        const errors = [];
        if (validator.isEmpty(petInputData.name) ||
            !validator.isLength(petInputData.name, { min: 3, max: 20 })) {
            errors.push(this.utilsService.lengthBetween('Name', '3', '20'));
        }
        if (imagesUploaded) {
            if (!imagesUploaded.every((element) => {
                return this.utilsService.fileFilter(element);
            })) {
                imagesUploaded.map((element) => {
                    this.utilsService.clearImage(element.path);
                })
                errors.push(`Files must be jpg, jpeg or png`);
            }
        }
        if (validator.isEmpty(petInputData.type) ||
            !validator.isLength(petInputData.type, { min: 2, max: 20 })) {
            errors.push(this.utilsService.lengthBetween('Type', '2', '20'));
        }
        if (errors.length > 0) {
            throw new ApolloError('Invalid input.', '422', {
                invalidArgs: errors,
            });
        }
    }

}
