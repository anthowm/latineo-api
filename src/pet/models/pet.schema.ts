import { Schema, model, Document } from "mongoose";
import { Pet } from "../../graphql.classes";
export interface PetDocument extends Pet, Document {

}

export const PetSchema: Schema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        type: {
            type: String,
            required: true,
        },
        imageUrls: {
            type: [Schema.Types.Mixed],
            required: true
        },
        creator: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }
    },
    {
        timestamps: true,
    },
);

export const PetModel = model(
    'Pet',
    PetSchema,
);