import { Resolver, Query, Args, Mutation, Context } from "@nestjs/graphql";
import { UtilsService } from "./../utils/utils.service";
import { UsersService } from "./../user/users.service";
import { JwtAuthGuard } from "./../auth/guards/jwt-auth.guard";
import { PetsService } from "./pets.service";
import { UseGuards } from "@nestjs/common";
import { Pet, PetInputData, User, PetInputDataUpdate, Upload } from "../graphql.classes";
import { filterPetInterface, PetsInterface, createPetInterface } from "./interfaces/pet.interface";
import mkdirp = require("mkdirp");
import { ApolloError } from "apollo-server-errors";

@Resolver('Pet')
export class PetsResolver {
    constructor(
        private readonly petsService: PetsService,
        private readonly usersService: UsersService,
        private readonly utilsService: UtilsService
    ) {
        mkdirp.sync(this.utilsService.UPLOAD_DIR);
    }

    @Query('pets')
    @UseGuards(JwtAuthGuard)
    async pets(
        @Args('page') page?: number,
        @Args('name') name?: string,
        @Args('type') type?: string
    ): Promise<PetsInterface> {
        if (!page) {
            page = 1;
        }
        const perPage = 8;
        let filter: filterPetInterface;
        let queryParams: filterPetInterface;
        if (name) {
            filter.name = { '$regex': name, '$options': 'i' };
            queryParams.name = name;
        }
        if (type) {
            filter.type = { '$regex': type, '$options': 'i' };
            queryParams.type = type;
        }
        const totalPets = await this.petsService.getTotalPets(filter);
        const pets = await this.petsService.getAllpets(page, perPage, filter);
        const filterPropertiesLength = filter ? Object.getOwnPropertyNames(filter).length : null;
        const result = {
            pets: pets,
            totalPets: totalPets,
            filterData: {
                isFilterData: filter ? (filterPropertiesLength > 0 ? true : false) : false,
                currentPage: page,
                queryParams: filter ? (filterPropertiesLength > 0 ? queryParams : null) : null
            }
        };
        return result;
    }

    @Query('pet')
    @UseGuards(JwtAuthGuard)
    async pet(@Args('id') id: number): Promise<Pet> {
        const pet = await this.petsService.findPetById(id);
        return pet;
    }

    @Mutation('createPet')
    @UseGuards(JwtAuthGuard)
    async createPet(
        @Args('petInputData') petInputData: PetInputData,
        @Context('req') req?: any
    ) {
        console.log("TCL: PetsResolver -> req", req)
        const user = await this.usersService.findUserById(req.userId);
        const imagesUploaded = await Promise.all(
            petInputData.imageUrls.map(this.utilsService.processUpload)
        );
        this.petsService.validationPet(petInputData, imagesUploaded);
        const pet: createPetInterface = {
            name: petInputData.name,
            type: petInputData.type,
            imageUrls: imagesUploaded,
            creator: user
        }
        const createdPet = await this.petsService.createPet(pet);
        user.pets.push(createdPet);
        user.save();
        return createdPet;
    }

    @Mutation('uploadFile')
    async uploadFile(@Args('file') file: Upload) {
        console.log("TCL: PetsResolver -> uploadFile -> file", file);
        return {
            id: '123454',
            path: 'www.wtf.com',
            filename: file.filename,
            mimetype: file.mimetype
        }
    }
    @Mutation('updatePet')
    @UseGuards(JwtAuthGuard)
    async updatePet(
        @Args('id') id: number,
        @Args('petInputDataUpdate') petInputDataUpdate: PetInputDataUpdate,
        @Context('req') req?: any
    ) {
        const pet = await this.petsService.findPetById(id);
        if (pet.creator._id.toString() !== req.userId.toString()) {
            throw new ApolloError('Not authorized!.', '403');
        }
        pet.name = petInputDataUpdate.name;
        pet.type = petInputDataUpdate.type;
        let imagesUploaded;
        if (petInputDataUpdate.imageUrls) {
            pet.imageUrls.map((element) => {
                this.utilsService.clearImage(element.path);
            });
            imagesUploaded = await Promise.all(
                petInputDataUpdate.imageUrls.map(this.utilsService.processUpload)
            );
            pet.imageUrls = imagesUploaded;
        } else {
            imagesUploaded = null;
        }
        this.petsService.validationPet(petInputDataUpdate, imagesUploaded);
        const updatedPet = await pet.save();
        return updatedPet;
    }

    @Mutation('deletePet')
    @UseGuards(JwtAuthGuard)
    async deletePet(
        @Args('id') id: number,
        @Context('req') req?: any
    ) {
        const pet = await this.petsService.findPetById(id);
        if (pet.creator.toString() !== req.userId.toString()) {
            throw new ApolloError('Not authorized!.', '403');
        }
        pet.imageUrls.map((element) => {
            this.utilsService.clearImage(element.path);
        });
        await this.petsService.findPetByIdAndRemove(id)
        let user = await this.usersService.findUserById(req.userId);
        user.pets = user.pets.filter((element) => {
            element._id !== id
        });
        await user.save();
        return true;
    }
}