import { Module } from '@nestjs/common';

import { UsersModule } from "./../user/users.module";
import { UsersService } from "./../user/users.service";
import { UtilsService } from "./../utils/utils.service";
import { PetSchema } from "./models/pet.schema";
import { DateScalar } from "./../scalars/date.scalar";
import { PetsService } from './pets.service';
import { PetsResolver } from './pets.resolver';
import { GlobalModule } from '../global/global.module';
import { MongooseModule } from '@nestjs/mongoose';



@Module({
    imports: [
        PetsModule,
        MongooseModule.forFeature([{ name: 'Pet', schema: PetSchema }]),
        GlobalModule,
        UsersModule
    ],
    providers: [
        UtilsService,
        PetsService,
        PetsResolver,
        DateScalar],
    exports: [PetsService]
})
export class PetsModule { } 