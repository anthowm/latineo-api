import { Pet, User } from "../../graphql.classes";

export interface filterPetInterface {
    name: Object;
    type: Object;
}

export interface PetsInterface {
    pets: Pet[];
    totalPets: number;
    filterData: {
        isFilterData: boolean,
        currentPage: number,
        queryParams: filterPetInterface
    }
}

export interface createPetInterface {
    name: String,
    type: String,
    imageUrls: Object,
    creator: User
}