import { Module } from '@nestjs/common';
import { UtilsService } from "./../utils/utils.service";
import { DateScalar } from "./../scalars/date.scalar";

import { UsersResolver } from './users.resolver';
import { UsersService } from './users.service';
import { GlobalModule } from '../global/global.module';
import { ConfigService } from '../config/config.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './models/user.schema';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    GlobalModule
  ],
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV}.env`),
    },
    UtilsService,
    UsersService,
    UsersResolver,
    DateScalar,
  ],
  exports: [UsersService]

})
export class UsersModule { }
