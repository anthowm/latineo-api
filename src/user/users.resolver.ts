
import { UsersService } from './users.service';
import { UserDocument } from './models/user.schema';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';

import { User, CreateUserInput, UpdateUserInput } from '../graphql.classes';
import { UserInputError, ValidationError } from 'apollo-server-core';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

@Resolver('User')
export class UsersResolver {
  constructor(private readonly usersService: UsersService) { }

  @Query('users')
  @UseGuards(JwtAuthGuard)
  async users() {
    return await this.usersService.getAllUsers();
  }

  @Query('user')
  @UseGuards(JwtAuthGuard)
  async user(
    @Args('username') username?: string,
    @Args('email') email?: string,
  ): Promise<User> {
    let user: User | undefined;
    if (username) {
      user = await this.usersService.findOneByUsername(username);
    } else if (email) {
      user = await this.usersService.findOneByEmail(email);
    } else {
      // Is this the best exception for a graphQL error?
      throw new ValidationError('A username or email must be included');
    }

    if (user) return user;
    throw new UserInputError('The user does not exist');
  }

  @Query('forgotPassword')
  async forgotPassword(@Args('email') email: string): Promise<void> {
    const worked = await this.usersService.forgotPassword(email);
  }

  // What went wrong is intentionally not sent (wrong username or code or user not in reset status)
  @Mutation('resetPassword')
  async resetPassword(
    @Args('username') username: string,
    @Args('code') code: string,
    @Args('password') password: string,
  ): Promise<User> {
    const user = await this.usersService.resetPassword(
      username,
      code,
      password,
    );
    if (!user) throw new UserInputError('The password was not reset');
    return user;
  }

  @Mutation('createUser')
  async createUser(
    @Args('createUserInput') createUserInput: CreateUserInput,
  ): Promise<User> {
    let createdUser: User | undefined;
    console.log("TCL: UsersResolver -> constructor -> createdUser", createdUser)
    try {
      createdUser = await this.usersService.create(createUserInput);
    } catch (error) {
      throw new UserInputError(error.message);
    }
    return createdUser;
  }

  @Mutation('updateUser')
  @UseGuards(JwtAuthGuard)
  async updateUser(
    @Args('username') username: string,
    @Args('fieldsToUpdate') fieldsToUpdate: UpdateUserInput,
  ): Promise<User> {
    let user: UserDocument | undefined;
    try {
      user = await this.usersService.update(username, fieldsToUpdate);
    } catch (error) {
      throw new ValidationError(error.message);
    }
    if (!user) throw new UserInputError('The user does not exist');
    return user;
  }
}
